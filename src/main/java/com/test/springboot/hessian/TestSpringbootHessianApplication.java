package com.test.springboot.hessian;

import com.test.springboot.hessian.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianServiceExporter;

@SpringBootApplication
public class TestSpringbootHessianApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestSpringbootHessianApplication.class, args);
    }

    @Autowired
    private HelloWorldService helloWorldService;

    @Bean(name = "/hello/world/service")
    public HessianServiceExporter accountService(){
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(helloWorldService);
        exporter.setServiceInterface(HelloWorldService.class);
        return exporter;
    }

}

