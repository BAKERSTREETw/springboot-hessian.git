package com.test.springboot.hessian.service;

public interface HelloWorldService {
    String sayHello(String name);
}
